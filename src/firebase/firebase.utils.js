import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyBgZJ_CVKOg-mLWspwWGbiMm_NslnpuwPg",
    authDomain: "readboot.firebaseapp.com",
    databaseURL: "https://readboot.firebaseio.com",
    projectId: "readboot",
    storageBucket: "readboot.appspot.com",
    messagingSenderId: "81067896228",
    appId: "1:81067896228:web:f5ce02294f6815e6396531",
    measurementId: "G-WQ0M1F9LYB"
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
